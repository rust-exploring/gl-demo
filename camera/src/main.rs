extern crate nalgebra_glm as glm;
mod mesh;
mod shader;
mod camera;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use mesh::Mesh;
use shader::Shader;
use camera::Camera;

#[derive(PartialEq)]
struct CriticalError(String);

type ExitStatus = Result<(), CriticalError>;

impl From<sdl2::video::WindowBuildError> for CriticalError {
    fn from(e: sdl2::video::WindowBuildError) -> Self {
        CriticalError(e.to_string())
    }
}

// We could also move out the source into its own file and load it at compile-time with std::include_str!
const VSHADER: &'static str = r#"
    #version 330
    layout (location = 0) in vec3 pos;
    out vec4 vCol;
    uniform mat4 model;
    uniform mat4 projection;
    uniform mat4 view;
    void main() {
        gl_Position = projection * view * model * vec4(pos, 1.0);
        vCol = vec4(clamp(pos, 0.0f, 1.0f), 1.0f);
    }
"#;

const FSHADER: &'static str = r#"
    #version 330
    in vec4 vCol;
    out vec4 colour;
    void main() {
        colour = vCol;
    }
"#;

fn create_triangle() -> Mesh {
    let elements = [      
        0, 3, 1u32,
        1, 3, 2,
        2, 3, 0,
        0, 1, 2,
    ];
    let vertices = [
        -1f32, -1f32, 0f32,
         0f32, -1f32, 1f32,
         1f32, -1f32, 0f32,
         0f32,  1f32, 0f32,
    ];

    Mesh::new(&vertices, &elements)
}


const SUCCESS: ExitStatus = Ok(());

fn launch() -> ExitStatus {
    let sdl = sdl2::init().map_err(CriticalError)?;
    let video = sdl.video().map_err(CriticalError)?;
    let timer = sdl.timer().map_err(CriticalError)?;
    let window = video.window("glDemo", 1280, 1080)
        .position_centered()
        .fullscreen_desktop()
        .opengl()
        .build()?;

    let _gl_context = window.gl_create_context().map_err(CriticalError)?;
    let (viewport_width, viewport_height) = window.drawable_size();
    gl::load_with(|s| video.gl_get_proc_address(s) as *const std::os::raw::c_void);

    unsafe {
        gl::Enable(gl::DEPTH_TEST);
        gl::ClearColor(0.0, 0.0, 0.0, 1.0);
        gl::Viewport(0, 0, viewport_width as i32, viewport_height as i32);
    }

    let mesh = create_triangle();
    let shader = Shader::new(VSHADER, FSHADER);
    let projection = glm::perspective(viewport_width as f32 / viewport_height as f32, 45.0f32, 0.1f32, 100.0f32);
    let mut camera = Camera::new(glm::vec3(0f32, 0f32, 0f32), glm::vec3(0f32, 1f32, 0f32), -90f32, 0f32, 3.0f32, 0.05f32);

    let mut now = timer.performance_counter();

    println!("Quit with ESC.");

    let mut pressed = [false; 4];
    let mut event_pump = sdl.event_pump().map_err(CriticalError)?;
    'running: loop {
        for ev in event_pump.poll_iter() {
            match ev {
                Event::Quit {..} => break 'running,
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => break 'running,
                Event::KeyDown { keycode: Some(Keycode::W), .. } => pressed[0] = true,
                Event::KeyDown { keycode: Some(Keycode::S), .. } => pressed[1] = true,
                Event::KeyDown { keycode: Some(Keycode::A), .. } => pressed[2] = true,
                Event::KeyDown { keycode: Some(Keycode::D), .. } => pressed[3] = true,

                Event::KeyUp { keycode: Some(Keycode::W), .. } => pressed[0] = false,
                Event::KeyUp { keycode: Some(Keycode::S), .. } => pressed[1] = false,
                Event::KeyUp { keycode: Some(Keycode::A), .. } => pressed[2] = false,
                Event::KeyUp { keycode: Some(Keycode::D), .. } => pressed[3] = false,

                Event::MouseMotion { xrel: x, yrel: y, .. } =>
                    camera.move_orientation(x, y),
                _ => {},
            }
        }

        let last = now.clone();
        now = timer.performance_counter();
        let delta = (now - last) as f32 / (timer.performance_frequency() as f32);

        if pressed[0] {
            camera.move_forward(delta)
        }
        if pressed[1] {
            camera.move_backward(delta)
        }
        if pressed[2] {
            camera.move_left(delta)
        }
        if pressed[3] {
            camera.move_right(delta)
        }

        let mut model = glm::identity::<f32, glm::U4>();
        model = glm::translate(&model, &glm::vec3(0f32, 0f32, -2.5f32));
        //model = glm::rotate(&model, f32::to_radians(angle), &glm::vec3(0f32, 1f32, 0f32));
        model = glm::scale(&model, &glm::vec3(0.4f32, 0.4f32, 1.0f32));

        let view = camera.view_matrix();

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            let launch = shader.activate();
            launch.uniform(shader.model_location(), &model);
            launch.uniform(shader.projection_location(), &projection);
            launch.uniform(shader.view_location(), &view);
            mesh.render();
        }
        window.gl_swap_window();
    }
    SUCCESS
}

fn main() {
    match launch() {
        Err(CriticalError(e)) => panic!("Critical Error: {}", e),
        SUCCESS => {},
    }
}
