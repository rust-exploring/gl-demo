
pub struct Shader {
    id: u32,
    uniform_projection: i32,
    uniform_model: i32,
    uniform_view: i32,
}

pub struct Launch {}

impl Launch {
    pub fn uniform(&self, location: i32, transform: &glm::Mat4x4) {
         unsafe {
            gl::UniformMatrix4fv(location, 1, gl::FALSE, glm::value_ptr(&transform).as_ptr());
        }
    }
}

impl Drop for Launch {
    fn drop(&mut self) {
        unsafe {
            gl::UseProgram(0)
        }
    }
}

impl Shader {
    pub fn projection_location(&self) -> i32 {
        self.uniform_projection
    }

    pub fn model_location(&self) -> i32 {
        self.uniform_model
    }

    pub fn view_location(&self) -> i32 {
        self.uniform_view
    }

    pub fn activate(&self) -> Launch {
        unsafe {
            gl::UseProgram(self.id);
        }
        Launch {}
    }

    pub fn new(vertex_code: &str, fragment_code: &str) -> Self {
        unsafe {
            let id = gl::CreateProgram();

            Self::add_shader(id, vertex_code, gl::VERTEX_SHADER);
            Self::add_shader(id, fragment_code, gl::FRAGMENT_SHADER);

            let mut result = 0;
            let mut elog = [0i8; 1024];

            gl::LinkProgram(id);
            gl::GetProgramiv(id, gl::LINK_STATUS, &mut result);

            if result == 0 {
                gl::GetProgramInfoLog(id, std::mem::size_of_val(&elog) as i32, std::ptr::null_mut(), elog.as_mut_ptr());
                eprintln!("Error linking program: {}", std::ffi::CStr::from_ptr(elog.as_ptr()).to_str().unwrap());
            }

            gl::ValidateProgram(id);
            gl::GetProgramiv(id, gl::VALIDATE_STATUS, &mut result);
            if result == 0 {
                gl::GetProgramInfoLog(id, std::mem::size_of_val(&elog) as i32, std::ptr::null_mut(), elog.as_mut_ptr());
                eprintln!("Error validating program: {}", std::ffi::CStr::from_ptr(elog.as_ptr()).to_str().unwrap());
            }

            let uniform_model = gl::GetUniformLocation(id, "model\0".as_ptr() as *const i8);
            let uniform_projection = gl::GetUniformLocation(id, "projection\0".as_ptr() as *const i8);
            let uniform_view = gl::GetUniformLocation(id, "view\0".as_ptr() as *const i8);
            
            Shader {id, uniform_projection, uniform_model, uniform_view}
        }
    }

    unsafe fn add_shader(program: u32, code: &str, shadertype: u32) {
        let shader = gl::CreateShader(shadertype);
        let sources = [code.as_ptr() as *const i8];
        let lens = [code.len() as i32];
        gl::ShaderSource(shader, 1, sources.as_ptr(), lens.as_ptr());
        gl::CompileShader(shader);

        let mut result = 1;
        let mut elog = [0i8; 1024];

        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut result);
        if result == 0 {
            gl::GetShaderInfoLog(shader, std::mem::size_of_val(&elog) as i32, std::ptr::null_mut(), elog.as_mut_ptr());
            eprintln!("Error compiling program: {}", std::ffi::CStr::from_ptr(elog.as_ptr()).to_str().unwrap());
            return
        }
        gl::AttachShader(program, shader);
    }
}


