
pub struct Camera {
    position: glm::Vec3,
    front: glm::Vec3,
    right: glm::Vec3,
    up: glm::Vec3,
    world_up: glm::Vec3,

    yaw: f32,
    pitch: f32,

    move_speed: f32,
    turn_speed: f32,
}

impl Camera {
    pub fn new(position: glm::Vec3, world_up: glm::Vec3, yaw: f32, pitch: f32, move_speed: f32, turn_speed: f32) -> Self {
        let mut camera = Camera {
            position: position,
            world_up: world_up,
            yaw: yaw,
            pitch: pitch,
            up: glm::vec3(0f32, 0f32, 0f32),
            right: glm::vec3(0f32, 0f32, 0f32),
            front: glm::vec3(0.0, 0.0f32, -1.0f32),
            move_speed: move_speed,
            turn_speed: turn_speed,
        };
        camera.update();
        camera
    }

    pub fn view_matrix(&self) -> glm::Mat4x4 {
        glm::look_at(&self.position, &(self.position + self.front), &self.up)
    }

    pub fn update(&mut self) {
        self.front.x = f32::cos(self.yaw.to_radians()) * f32::cos(self.pitch.to_radians());
        self.front.y = f32::sin(self.pitch.to_radians());
        self.front.z = f32::sin(self.yaw.to_radians()) * f32::cos(self.pitch.to_radians());

        self.front = glm::normalize(&self.front);

        self.right = glm::normalize(&glm::cross::<f32, glm::U3>(&self.front, &self.world_up));
        self.up = glm::normalize(&glm::cross::<f32, glm::U3>(&self.right, &self.front));
    }

    pub fn move_orientation(&mut self, xrel: i32, yrel: i32) {
        self.yaw += xrel as f32 * self.turn_speed;
        self.pitch -= yrel as f32 * self.turn_speed;
        if self.pitch > 89.0 {
            self.pitch = 89.0
        }
        if self.pitch < -89.0 {
            self.pitch = -89.0
        }
        self.update()
    }

    pub fn move_forward(&mut self, delta: f32) {
        self.position += self.front * self.move_speed * delta;
    }

    pub fn move_backward(&mut self, delta: f32) {
        self.position -= self.front * self.move_speed * delta;
    }

    pub fn move_left(&mut self, delta: f32) {
        self.position -= self.right * self.move_speed * delta;
    }

    pub fn move_right(&mut self, delta: f32) {
        self.position += self.right * self.move_speed * delta;
    }
}
