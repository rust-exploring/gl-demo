extern crate nalgebra_glm as glm;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

#[derive(PartialEq)]
struct CriticalError(String);

type ExitStatus = Result<(), CriticalError>;

impl From<sdl2::video::WindowBuildError> for CriticalError {
    fn from(e: sdl2::video::WindowBuildError) -> Self {
        CriticalError(e.to_string())
    }
}

// We could also move out the source into its own file and load it at compile-time with std::include_str!
const VSHADER: &'static str = r#"
    #version 330
    layout (location = 0) in vec3 pos;
    uniform mat4 model;
    void main() {
        gl_Position = model * vec4(pos, 1.0);    
    }
"#;

const FSHADER: &'static str = r#"
    #version 330
    out vec4 colour;
    void main() {
        colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    }
"#;

unsafe fn add_shader(program: u32, code: &'static str, shadertype: u32) {
    let shader = gl::CreateShader(shadertype);
    let sources = [code.as_ptr() as *const i8];
    let lens = [code.len() as i32];
    gl::ShaderSource(shader, 1, sources.as_ptr(), lens.as_ptr());
    gl::CompileShader(shader);

    let mut result = 1;
    let mut elog = [0i8; 1024];

    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut result);
    if result == 0 {
        gl::GetShaderInfoLog(shader, std::mem::size_of_val(&elog) as i32, std::ptr::null_mut(), elog.as_mut_ptr());
        eprintln!("Error compiling program: {}", std::ffi::CStr::from_ptr(elog.as_ptr()).to_str().unwrap());
        return
    }
    gl::AttachShader(program, shader);
}

struct Program {
    program: u32,
    model_location: i32,
}

struct ProgramLauncher;

impl ProgramLauncher {
	pub fn launch(shader: &Program, model: &glm::Mat4x4) -> ProgramLauncher {
		unsafe {
			gl::UseProgram(shader.program);
			gl::UniformMatrix4fv(shader.model_location, 1, gl::FALSE, glm::value_ptr(&model).as_ptr());
		}
		ProgramLauncher {}
	}
}

impl Drop for ProgramLauncher {
	fn drop(&mut self) {
		unsafe { gl::UseProgram(0) }
	}
}

unsafe fn compile_shaders() -> Program {    
    let shader = gl::CreateProgram();

    add_shader(shader, VSHADER, gl::VERTEX_SHADER);
    add_shader(shader, FSHADER, gl::FRAGMENT_SHADER);

    let mut result = 0;
    let mut elog = [0i8; 1024];

    gl::LinkProgram(shader);
    gl::GetProgramiv(shader, gl::LINK_STATUS, &mut result);

    if result == 0 {
        gl::GetProgramInfoLog(shader, std::mem::size_of_val(&elog) as i32, std::ptr::null_mut(), elog.as_mut_ptr());
        eprintln!("Error linking program: {}", std::ffi::CStr::from_ptr(elog.as_ptr()).to_str().unwrap());
    }

    gl::ValidateProgram(shader);
    gl::GetProgramiv(shader, gl::VALIDATE_STATUS, &mut result);
    if result == 0 {
        gl::GetProgramInfoLog(shader, std::mem::size_of_val(&elog) as i32, std::ptr::null_mut(), elog.as_mut_ptr());
        eprintln!("Error validating program: {}", std::ffi::CStr::from_ptr(elog.as_ptr()).to_str().unwrap());
    }

    let model_location = gl::GetUniformLocation(shader, "model\0".as_ptr() as *const i8);

    Program { program: shader, model_location: model_location}
}

fn create_triangle() -> u32 {
    let vertices = [
        -1f32, -1f32, 0f32,
         1f32, -1f32, 0f32,
         0f32,  1f32, 0f32
    ];

    let mut vao = 0u32;
    let mut vbo = 0u32;

    unsafe {
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);

        gl::GenBuffers(1, &mut vbo);
        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(gl::ARRAY_BUFFER, std::mem::size_of_val(&vertices) as isize, vertices.as_ptr() as *mut std::ffi::c_void, gl::STATIC_DRAW);
        gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 0, std::ptr::null());
        gl::EnableVertexAttribArray(0);
        
        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        gl::BindVertexArray(0);
    }

    vao
}


const SUCCESS: ExitStatus = Ok(());

fn launch() -> ExitStatus {
    let sdl = sdl2::init().map_err(CriticalError)?;
    let video = sdl.video().map_err(CriticalError)?;
    let window = video.window("glDemo", 1920, 1080)
        .position_centered()
        .opengl()
        .build()?;

    let _gl_context = window.gl_create_context().map_err(CriticalError)?;
    let (viewport_width, viewport_height) = window.drawable_size();
    gl::load_with(|s| video.gl_get_proc_address(s) as *const std::os::raw::c_void);

    unsafe {
        gl::ClearColor(0.8, 0.8, 0.8, 1.0);
        gl::Viewport(0, 0, viewport_width as i32, viewport_height as i32);
    }

    let vao = create_triangle();
    let shader = unsafe { compile_shaders() };

    let tri_maxoffset = 0.7f32;
    let mut tri_offset = 0.0f32;    
    let mut tri_increment = 0.005f32;

    let mut angle = 0.0f32;
    let angle_increment = 0.1f32;

    let scale_max = 0.8f32;
    let scale_min = 0.4f32;
    let mut scale_increment = 0.001f32;
    let mut scale = scale_min;


    println!("Quit with ESC. Start moving ...");
    
    let mut event_pump = sdl.event_pump().map_err(CriticalError)?;
    'running: loop {
        for ev in event_pump.poll_iter() {
            match ev {
                Event::Quit {..} => break 'running,
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => break 'running,
                _ => {},
            }
        }

        if f32::abs(tri_offset) > tri_maxoffset {
            tri_increment = -tri_increment;
        }
        if scale < scale_min || scale > scale_max {
            scale_increment = -scale_increment;
        }
    
        if angle > 360.0f32 {
            angle -= 360.0f32;
        }

        tri_offset += tri_increment;
        angle += angle_increment;
        scale += scale_increment;

        let mut model = glm::identity::<f32, glm::U4>();
        model = glm::translate(&model, &glm::vec3(tri_offset, 0.0f32, 0.0f32));
        model = glm::rotate_z(&model, f32::to_radians(angle));
        model = glm::scale(&model, &glm::vec3(scale, scale, 1.0f32));        

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
			let _launcher = ProgramLauncher::launch(&shader, &model);
            gl::BindVertexArray(vao);
            gl::DrawArrays(gl::TRIANGLES, 0, 3);
            gl::BindVertexArray(0);
        }
        window.gl_swap_window();
    }
    SUCCESS
}

fn main() {
    match launch() {
        Err(CriticalError(e)) => panic!("Critical Error: {}", e),
        SUCCESS => {},
    }
}
